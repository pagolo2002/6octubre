<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of GrupoModel
 *
 * @author a023862857n
 */
namespace App\Models;
use CodeIgniter\Model;


class GrupoModel extends Model{
    
    protected $table = 'grupos';
    protected $primaryKey = 'id';
    protected $returnType = 'object';
}
