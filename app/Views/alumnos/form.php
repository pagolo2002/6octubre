<?php

?>
<html>
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

        <title><?= $title?></title>
    </head>
    <body>
        <h1 class="text-primary"><?= $title?></h1>
        <form action="<?= site_url('MuestraA')?>" method="post">
            <label for="texto" class="text-success">Introduce el texto a buscar: </label>
            <input type="text" name="texto" value="Pon un valor" id="texto" />
            <input type="submit" name="enviar" value="Enviar" />
        </form>
    </body>
</html>
    
